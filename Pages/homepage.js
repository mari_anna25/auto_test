
const HomePage = function() { 
       this.emailField = element(by.model('form.email'));
       this.loginText = element(by.buttonText('Log in'));
       this.passwordField = element(by.model('form.password'));
       this.eyeShowPassword = element(by.className('icon icon-eye'));
       this.loginButton = element(by.xpath('/html/body/div[1]/div/ui-view/div/ng-include/div/div/form/div[3]/button'));
       this.loginChangedToEmail = element(by.xpath('/html/body/div[1]/div/div/header/div/div/div/button/span'));
       this.notification = element(by.xpath('/html/body/ul/li/div/div/div[2]/div'));
       this.triangle = element(by.xpath('/html/body/div[1]/div/div/header/div/div/div/button'));
       this.profile = element(by.xpath('/html/body/div[1]/ssls-header/div/div/header/div/div/div[1]/div/ul/li[2]/a'));
       this.logOut = element(element(by.buttonText(' Log out')));
       this.profileData = element.all(by.className('text ng-binding')).getText();
       this.dropdown = element(by.className('ssls-dropdown__holder ssls-dropdown__holder--toolbar'));

       this.get =  function() {
         browser.waitForAngularEnabled(false);
         protractor.ignoreSynchronization = true;
         browser.driver.manage().window().maximize();
         browser.get('https://www.sbzend.ssls.com');
      };
}
module.exports = new HomePage();