exports.config = {
   framework: 'jasmine',
  // seleniumAddress: 'http://localhost:4444/wd/hub',
   directConnect: true,
   specs: ['spec.js'],
   jasmineNodeOpts: {
      defaultTimeoutInterval: 10000
    },
    onPrepare: async () => {
      await browser.waitForAngularEnabled(false);
  }
}
