1) Ensure that NPM is installed on your machine. NPM is installed with Node.js. 
Type npm -v in console(cmd | PowerShell | Terminal) to verify successful installation.
2) Clone the project and navigate to the cloned folder.
3) Run npm install to install all required packages.
4) Install protractor
5) To run the tests, use the command protractor config.js